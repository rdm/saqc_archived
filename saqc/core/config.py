#! /usr/bin/env python
# -*- coding: utf-8 -*-


class Fields:
    VARNAME = "varname"
    START = "start_date"
    END = "end_date"
    TESTS = "test*"
    PLOT = "plot"
    LINENUMBER = "line"


class Params:
    FLAG_GENERIC = "flagGeneric"
    FUNC = "func"
